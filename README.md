# sisop-praktikum-modul-2-2023-AM-B06

Anggota kelompok B06 adalah :

|        Nama           | NRP|
| ---                   |--- |
|Glenaya                |5025211202 |
|Gracetriana Survianta  Septinaputri | 5025211199 |
|Ken Anargya Alkausar   | 5025211168 |

# Laporan Resmi Praktikum Modul 2 Sisop

## Soal Nomor 1
 
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
>>- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
>>- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
>>- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
>>- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan :
untuk melakukan zip dan unzip tidak boleh menggunakan system

### Pembahasan 1A
Soal meminta untuk mendownload file pada folder lalu unzip folder. Kode yang digunakan adalah 
```c
void download_file(char link[], char file_name[]) {

	int status;

	if (fork()==0) {
	char *argv[] = {"wget", link, "-O", file_name, NULL};
	execv("/bin/wget", argv);
	}
		while (wait(&status) >= 0);
		return;
}
```
Kode ini merupakan sebuah fungsi yang bernama ```download_file``` yang menerima dua parameter string yaitu ```link``` sebagai link download dan ```file_name``` sebagai nama untuk folder yang didownload. Fungsi ini bertujuan untuk mendownload file dari suatu link dengan menggunakan perintah wget.

- Pertama yang dilakukan adalah mendeklarasi satu integer yaitu ```status``` sebagai status keluaran dari proses yang dijalankan.
- Kedua, terdapat fungsi ```fork()``` untuk membuat _child process_. Jika nilai ```fork()```adalah 0, maka _child process_ akan dijalankan. Sehingga argumen perintah ```wget``` diatur di dalam array ```argv```.
- Ketiga terdapat fungsi ```execv()``` yang berguna untuk menjalankan perintah ```wget``` dengan argumen yang ditentukan dan menjalankan program yang berada di path tertentu. ```NULL``` menandakan akhir dari array argv.
- Jika fungsi ```fork()``` tidak sama dengan 0, maka yang dijalankan adalah _parent process_, sehingga fungsi ```wait()``` akan dipanggil untuk menunggu _child process_ selesai. Ketika selesai status akan disimpan, lalu kontrol akan kembali ke _parent process_ sambil menunggu _child process_ selesai.

```c
void unzip(char zip_name[], char folder_name[]) {

	int status;

	if (fork()==0) {
	char *argv[] = {"unzip", zip_name, "-d", folder_name, NULL};
	execv("/usr/bin/unzip", argv);
	}
		while (wait(&status) >= 0);
		return;
}
```
Kode ini merupakan sebuah fungsi yang bernama ```unzip``` digunakan untuk unzip sebuah file zip ke dalam folder. Fungsi ini menggunakan dua parameter string yaitu ```zip_name``` sebagai nama file zip yang akan di-unzip dan ```folder_name``` sebagai nama folder setelah file tersebut di-zip

Cara kerja kode ini juga mirip dengan fungsi ```download_file``` sebelumnya.
- Pertama yang dilakukan adalah mendeklarasi satu integer yaitu ```status``` sebagai status keluaran dari proses yang dijalankan.
- Kedua digunakan fungsi ```fork()``` untuk membuat _child process_ baru yang akan menjalankan perintah ```unzip``` menggunakan ```execv()```. 
- Dalam fungsi ini terdapat perintah ```-d``` untuk menentukan direktori mana sebagai tempat file setelah di-unzip.
- Untuk perintah ```wait()``` juga sama dengan fungsi yang terdapat di ```download_file```. _Parent process_ akan menunggu _child process_ selesai, lalu fungsi akan mengembalikan nilai.

### Pembahasan 1B
Soal meminta untuk memilih secara acak file gambar dari hasil yang sudah di-zip. Kode yang digunakan adalah 
```c
void select_random_file(char folder_path[]) {

	char buf[PATH_MAX];
	DIR* dir;
	struct dirent *ent;
	srand(time(NULL));
    	int n_files = 0;

    	dir = opendir(folder_path);
    	if (dir == NULL) {
        	fprintf(stderr, "Error opening directory: %s\n", folder_path);
        	return;
		}

    	while ((ent = readdir(dir)) != NULL) {
        	if (ent -> d_type == DT_REG) {
            	n_files++;
            		if (rand()%n_files == 0) {
                	snprintf (buf, sizeof(buf), "%s", ent -> d_name);
            		}
        	}
    	}
    	closedir(dir);
    	printf ("Penjagaan Hewan : %s\n",buf);
}
```
Kode ini merupakan sebuah fungsi yang bernama ```select_random_file``` yang digunakan untuk memilih secara acak sebuah file dalam suatu folder tertentu. Fungsi ini menerima sebuah parameter string yaitu ```folder_path``` yang berisi path folder yang berisi file yang akan dipilih secara acak.

- Pertama menginisialisasi ```srandom``` yaitu random generator dengan nilai dari fungsi ```time()``` berfungsi untuk mengembalikan waktu.
- Kedua, program akan membuka folder menggunakan ```opendir()```. Jika folder tidak bisa dibuka, maka fungsi akan mengeluarkan pesan error dan mengembalikan nilai.
- Ketiga program akan membaca satu per satu file yang terdapat di folder menggunakan fungsi ```readdir``` dan menyimpan jumlah file yang ditemukan ke dalam variabel ```n_files```.
- Setiap kali program menemukan file bertipe regular(DT_REG), maka program akan menghitung angka acak menggunakan fungsi ```rand``` dan membandingkan dengan ```n_files```. Jika angka acak yang dihasilkan adalah 0, maka program akan menyimpannya ke dalam buffer ```buf``` dengan menggunakan ```snprintf()```
- Terakhir, ketika semua file selesai diproses program akan menutup folder menggunakan ```closedir()``` lalu mengeluarkan nama file yang telah dipilih secara acak.

### Pembahasan 1C
Soal meminta untuk membuat direktori baru yang bernama **HewanDarat, HewanAmphibi** dan **HewanAir**. Setelah membuat direktori baru akan dilakukan filter atau pemindahan file ke dalam direktori tersebut berdasarkan tempat tinggalnya.
Kode yang digunakan adalah

```c
void new_directory_HewanDarat() {

        if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", NULL};
        execv("/bin/mkdir", argv);
	}
}
```
Kode ini merupakan sebuah fungsi yang bernama ```new_directory_HewanDarat``` yang berfungsi untuk membuat sebuah direktori baru bernama **HewanDarat**.

Cara kerja kode ini juga mirip dengan dua fungsi sebelumnya yaitu ```unzip``` dan ```download_file```.
- Pertama fungsi ini menggunakan ```fork()``` untuk membuat sebuah _child process_ yang akan menjalankan perintah ```mkdir```.
- Kedua, fungsi ini menginisialisasi sebuah array yang berisi argumen ```mkdir``` yang berguna untuk membuat direktori baru, lalu terdapat argumen ```-p``` yang berguna untuk membuat direktori secara rekursif jika diperlukan. Ketiga terdapat argumen sebagai nama direktori baru yang dibuat yaitu ```HewanDarat```. Terakhir terdapat argumen NULL yang menunjukan akhir dari array argumen.
- Ketiga, fungsi ```execv()``` digunakan untuk mengeksekusi perintah ```mkdir```. 

Untuk membuat direktori **HewanAir** dan **HewanAmphibi** cara kerjanya sama dengan **HewanDarat** dengan menggunakan kode sebagai berikut :

**Direktori Baru HewanAmphibi**

```c
void new_directory_HewanAmphibi() {

        if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
        }
}
```
**Direktori Baru HewanAir**
```c
void new_directory_HewanAir() {

	if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
        }

}
```
Pada catatan soal nomor 1 hanya zip dan unzip yang tidak boleh menggunakan system, sehingga pada melakukan filter atau pemindan file saya menggunakan system seperti di bawah ini:

```c
void move_filter_HewanDarat(){
	system("find . -name \"*_darat*\" -type f | awk '{ \
    		if (!system(\"[ ! -e HewanDarat/\" $0 \" ]\")) \
        	system(\"mv \" $0 \" HewanDarat/\") \
	}'");
}
```
Kode ini merupakan sebuuah fungsi bernama ```move_filter_HewanDarat``` yang berguna untuk memindahkan file yang terdapat di direktori saat ini ke direktori **HewanDarat**.

- Terdapat perintah ```system()``` untuk menjalankan perintah ```find . -name \"*_darat*\" -type f```. Perintah ini berguna untuk mencari semua file yang memiliki string ```_darat``` dalam direktori saat ini.
- Selain itu di fungsi menggunakan perintah ```awk``` untuk memproses output dari perintah ```find```. Perintah ```awk``` digunakan untuk mengecek apakah file sudah ada di dalam direktori **HewanDarat**. Jika belum maka perintah ```mv``` dijalankan untuk memindahkan file tersebut.

Untuk melakukan filter atau pemindahan file pada **HewanAir** dan **HewanAmphibi** cara kerjanya sama dengan **HewanDarat**. Kode yang digunakan sebagai berikut :

**Filter HewanAir**
```c
void move_filter_HewanAir(){
        system("find . -name \"*_air*\" -type f | awk '{ \
                if (!system(\"[ ! -e HewanAir/\" $0 \" ]\")) \
                system(\"mv \" $0 \" HewanAir/\") \
        }'");
}
```

**Filter HewanAmphibi**
```c
void move_filter_HewanAmphibi(){
        system("find . -name \"*_amphibi*\" -type f | awk '{ \
                if (!system(\"[ ! -e HewanAmphibi/\" $0 \" ]\")) \
                system(\"mv \" $0 \" HewanAmphibi/\") \
        }'");
}
```


### Pembahasan 1D 
Soal ini meminta untuk zip direktori baru yang sudah dibuat di soal 1C. Kode yang digunakan adalah 
```c
void zip_HewanDarat() {

	if (fork() == 0) {
	char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
	execv("/bin/zip", argv);
	}

}
```
Kode ini merupakan sebuah fungsi yang bernama ```zip_HewanDarat``` digunakan untuk zip satu direktori yang bernama HewanDarat. Cara kerja fungsi ini hampir sama dengan fungsi ```unzip``` di bagian pembahasan 1A.
- Pertama fungsi ini menggunakan ```fork()``` untuk membuat _child process_. Jika nilai sama dengan 0, maka proses yang sedang berjalan adalah _child process_
- Kedua, _child process_ akan menjalankan argumen ```zip``` dengan menggunakan ```execv()```. Selain itu digunakan argumen ```-r``` sebagai argumen untuk melakukan kompresi dan argumen bernilai ```NULL``` untuk menandakan akhir dari array tersebut.

Untuk melakukan zip pada direktori **HewanAir** dan **HewanAmphibi** caranya sama dengan **HewanDarat**. Kodenya adalah sebagai berikut

**Zip HewanAir**
```c
void zip_HewanAir() {

	if (fork() == 0) {
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
        }
}
```

**zip HewanAmphibi**
```c
void zip_HewanAmphibi() {

        if (fork() == 0) {
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
        }
}
```
#### Pembahasan Main() Untuk Soal Nomor 1

```c
int main () {
	download_file("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq","binatang.zip");
	unzip("binatang.zip","f_binatang");

	char* folder_path = "/home/glenaya/Sisop/prak2/f_binatang";
	select_random_file(folder_path);

	new_directory_HewanDarat();
	new_directory_HewanAmphibi();
	new_directory_HewanAir();

	move_filter_HewanDarat();
	move_filter_HewanAir();
	move_filter_HewanAmphibi();

	zip_HewanDarat();
 	zip_HewanAmphibi();
    	zip_HewanAir();
}
```
Pada soal 1A diminta untuk mendownload file melalui link. Sehingga fungsi ```download_file()``` yang berisi parameter link dan nama foldernya yaitu ```binatang.zip```. Selain itu, soal 1A juga meminta untuk unzip folder yang sudah didownload. Fungsi ```unzip()``` pun dipanggil dengan dua parameter yaitu nama folder yang akan diunzip yaitu ```binatang.zip``` dan nama folder setelah unzip yaitu ```f_binatang```.

Pada soal 1B diminta untuk memilih file secara acak. Maka fungsi ```select_random_file()``` dipanggil dengan parameter ```folder_path```. Sebelum itu ```folder_path``` akan dideklarasi terlebih dahulu. Path folder saya adalah ```/home/glenaya/Sisop/prak2/f_binatang```.

Pada soal 1C diminta untuk membuat direktori baru dan melakukan filter. Sehingga semua fungsi di bawah ini dipanggil.
```c
	new_directory_HewanDarat();
	new_directory_HewanAmphibi();
	new_directory_HewanAir();

	move_filter_HewanDarat();
	move_filter_HewanAir();
	move_filter_HewanAmphibi();
```
Begitu juga untuk soal 1D, soal 1D meminta untuk melakukan zip kembali hasil direktori yang sudah dilakukan filter. Sehingga fungsi dibawah ini dipanggil
```c
	zip_HewanDarat();
 	zip_HewanAmphibi();
    	zip_HewanAir();
```





## Soal Nomor 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
>>- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
>>- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
>>- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
>>- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
>>- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

	Catatan : Tidak boleh menggunakan system() 
		  Proses berjalan secara daemon
		  Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### Pembahasan 2A
Membuat program c yang selama tiap 30 detik membuat folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
```c
pid_t child_id;
	int status;
	child_id = fork();
        time_t raw;
        struct tm *timeinfo;
        char dates[40];
        time(&raw);
        timeinfo = localtime(&raw);
        strftime(dates, sizeof(dates), "%Y-%m-%d_%H:%M:%S", timeinfo); 
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
		char *argv[4] = {"mkdir", "-p", dates, NULL};
		execv("/bin/mkdir", argv); 
```
dengan menggunakan fungsi localtime dan strftime untuk membuat string timestamp dengan format `[YYYY-MM-dd_HH:mm:ss]`. Kemudian pada bagian if (child_id == 0), program melakukan fork untuk membuat child process dan memanggil execv dengan parameter "/bin/mkdir" dan nama folder yang diinginkan yang merupakan string timestamp tersebut. Sehingga setiap 30 detik, program akan membuat folder baru dengan nama timestamp.

### Pembahasan 2B
Pada setiap folder diisi dengan 15 gambar yang di download dari https://picsum.photos/ , yang di download setiap 5 detik. Tiap gambar berukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp.
```c
while (i<15)
			{
        			time_t rw;
        			struct tm *timeinfo2;
        			char tgl[40];
        			time(&rw);
        			timeinfo2 = localtime(&rw);
        			strftime(tgl, sizeof(tgl), "%Y-%m-%d_%H:%M:%S", timeinfo2); 
				char adrs[50];
				sprintf(adrs, "https://picsum.photos/%ld", ((rw%1000)+100));
				pid_t child_id_3;
				child_id_3 = fork();
				if (child_id_3 == 0) {
					chdir(dates);
				//	execl("/usr/bin/wget", "wget", "-O", tgl, adrs, NULL);
					execl("/usr/bin/wget", "wget", "-q","-O", tgl, adrs, NULL);
				}
				i++;
				sleep(5);
			}
```
Setiap gambar diberi nama dengan format timestamp. Proses pengunduhan gambar dilakukan dengan menggunakan program wget melalui fungsi exec. Sebelum melakukan pengunduhan, program akan menjalankan perintah wget dengan argumen "-q" agar pengunduhan dilakukan secara diam-diam, "-O" untuk memberi nama file dengan format timestamp, dan URL dari gambar yang diunduh. 

### Pembahasan 2C
saat folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di hapus (sehingga hanya menyisakan .zip).
```c
char zip_name[50];
			sprintf(zip_name, "%s.zip", dates);
                        char *argv2[5] = {"zip", "-rm", zip_name, dates, NULL};
                        execv("/usr/bin/zip", argv2);

```
Proses pembuatan file zip dilakukan dengan menggunakan program zip melalui fungsi execv. Argumen yang digunakan adalah "-rm" agar folder yang telah di-zip dapat langsung dihapus.
n-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

### Pembahasan 2D
```c
void createKiller(char *argv[], pid_t pid, pid_t sid)
{
    FILE *fileptr = fopen("killer.c", "w");

    // Killer.c
    char input[1024] = ""
    "#include <unistd.h>\n"
    "#include <wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
    "}\n";

    // Compile killer.c
    pid = fork();
    if(pid == 0)
    {    
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", command);
    }
    while(wait(NULL) != pid);

    // Remove killer.c
    pid = fork();
    if (pid == 0) {
    	char *argv[] = {"rm", "killer.c", NULL};
    	execv("/bin/rm", argv);
    }
    while(wait(NULL) != pid);
}
```
membuat file killer.c yang akan berisi program untuk menghentikan program yang sedang berjalan. Fungsi ini menerima tiga argumen, yaitu argv, pid, dan sid. argv berisi argumen-argumen yang diberikan pada program saat dijalankan, pid berisi pid (process id) dari proses yang sedang berjalan, dan sid berisi sid (session id) dari proses yang sedang berjalan. Setelah menuliskan program pada file killer.c, program akan melakukan kompilasi file killer.c menggunakan perintah gcc dan kemudian menghapus file killer.c

### Pembahasan 2E
```c
 	// Mode A
    char command[1024];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fileptr, input, command, "/usr/bin/pkill");
    }

    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fileptr, input, command, "/bin/kill");
    }

    fclose(fileptr);

```
Kode ini bekerja dengan cara membuka file killer.c dan menuliskan program yang akan digunakan untuk menghentikan proses yang sedang berjalan. Program ini akan menggunakan dua mode, yaitu Mode A dan Mode B. 
- Pada Mode A, program akan menggunakan perintah pkill untuk menghentikan proses. Program akan menuliskan perintah char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n" pada file killer.c, di mana %d akan diganti dengan nilai sid. Setelah itu, program akan menjalankan perintah execv untuk menjalankan program pkill.
- Pada Mode B, program akan menggunakan perintah kill untuk menghentikan proses. Program akan menuliskan perintah char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n" pada file killer.c, di mana %d akan diganti dengan nilai getpid() untuk menghentikan proses yang sedang berjalan. Setelah itu, program akan menjalankan perintah execv untuk menjalankan program kill.











## Soal Nomor 3

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
>- Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
>- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
>- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
>- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

### Pembahasan 3A

Diminta untuk membuat program ```filter.c``` yang mengunduh file database pemain bola, extract "players.zip" dan menghapus file zipnya

```c
void for_exec(char path[], char *argv[])
{
  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execv(path,argv);
    exit(EXIT_SUCCESS);
  }
  else
  {
    while(wait(&status) > 0)
        ;
    return;
  }
}
```
Pertama-tama, untuk mempermudah penggunaan fork() dan exec() secara bersamaan, dibuat fungsi `for_exec` dengan parameter `char path[]` dan `char *argv[]`. Di dalam fungsi ini, deklarasi variabel child_id dan status terlebih dahulu kemudian lakukan `fork()` pada `child_id`. Jika `child_id < 0` maka akan keluar dari program dan jika `child id == 0` maka akan menjalankan perintah `execv` dan keluar dari program setelah sukses dijalankan. Selain kedua kondisi tadi, program akan menunda proses parent sampai child process selesai dieksekusi.
```c
void file_download(char urlFile[], char nameFile[])
{
  char *argv[] = {"wget", urlFile, "-O", nameFile, NULL};
  for_exec("/usr/bin/wget", argv);
}
```
Fungsi `file_download` digunakan untuk mengunduh file database pemain bola. Di dalam fungsi, dideklarasikan `char *argv[]` yang berisi command `wget`, urlFile (url file yang akan didownload), `"-O"`, nameFile (nama file yang akan diunduh), dan nilai NULL. Kemudian, fungsi `for_exec()` dipanggil untuk menjalankan proses `fork()` dan `exec()` secara bersamaan. Di dalam `for_exec()` terdapat path dari command `wget` dan argumen argv yang telah dideklarasi tadi.
```c
void mk_dir(char dir[])
{
  char *argv[] = {"mkdir", "-p", dir, NULL};
  for_exec("/usr/bin/mkdir", argv);
}
```
Fungsi `mk_dir` ini sama seperti fungsi `file_download`, bedanya fungsi ini digunakan untuk membuat directory baru dengan commandnya yaitu `"mkdir".
```c
void unzip(char nameFile[], char destDir[])
{
  char *argv[] = {"unzip", "-q", nameFile, "-d", destDir, NULL};
  for_exec("/usr/bin/unzip", argv);
}
```
Fungsi `unzip` ini juga memiliki isi kurang lebih sama dengan fungsi file_download dan mk_dir, hanya saja fungsi ini digunakan untuk mengextract file yang telah didownload kemudian memasukkan hasil ekstraksi file tersebut ke direktori yang diinginkan. Pada fungsi ini, argumennya (argv) berisi command `unzip -q` untuk melakukan ekstraksi, nameFile (nama file yang akan diextract), `-d` untuk mengarahkan ke direktori yang dituju, destDir yaitu direktori tujuan, dan nilai NULL. Sama seperti fungsi sebelumnya, dipanggil fungsi `for_exec()` dengan parameter path unzip dan argv.
```c
void rm_file(char nameFile[])
{
  char *argv[] = {"rm", nameFile, NULL};
  for_exec("/usr/bin/rm", argv);
}

void mv_file(char *source, char *dest)
{
  char *argv[] = {"mv", source, dest, NULL};
  for_exec("/usr/bin/mv", argv);
}
```
Kedua fungsi di atas juga sama seperti fungsi-fungsi sebelum ini, perbedaannya adalah `rm_file` digunakan untuk menghapus file (dalam nomor ini akan menghapus players.zip) dan `mv_file` akan memindahkan file.

#### Pemanggilan Fungsi
Untuk menjalankan fungsi-fungsi di atas, dilakukan pemanggilan di dalam `int main()`. Pada poin 3A ini, pemanggilan dilakukan dengan cara sebagai berikut:
```c
int main(){
  file_download("https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download","players.zip");
  mk_dir("modul2");
  unzip("players.zip","modul2/");
  rm_file("players.zip");
...
```
`file_download` akan mengunduh file dari url tersebut dan memasukkannya ke `"players.zip"`, `mk_dir` akan membuat directory baru yaitu modul2, `unzip` akan mengekstraksi players.zip dan memasukkannya ke directory modul2, dan players.zip tadi akan dihapus dengan melakukan pemanggilan fungsi `rm_file`.

### Pembahasan 3B

Diminta untuk menghapus semua pemain yang bukan berasal dari Manchester United

Melakukan deklarasi path dimana data pemain disimpan terlebih dahulu.
```c
char players_path[] = "/home/grace/modul2/players";
```

```c
void rm_notManUtd()
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(players_path);
...
```
Membuat fungsi `rm_notManUtd()` yang berisi program untuk menghapus pemain yang bukan dari ManUtd, fungsi ini juga akan melakukan directory listing untuk melihat file apa saja yang ada pada suatu directory. Di dalam fungsi ini, dilakukan deklarasi array path, struct dirent *dp, dan DIR *dir yang akan membuka directory dengan path yang dimiliki players_path.
```c
    ...
    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (dp->d_type == DT_REG && strstr(dp->d_name, "ManUtd") == NULL)
        {
            pid_t pid;
            sprintf(path,"%s/%s",players_path,dp->d_name);
            pid = fork();
            if(pid == 0)
            {
              rm_file(path);
              exit(EXIT_SUCCESS);
            }
            waitpid(pid, NULL, 0);
        }
    }

    closedir(dir);
}
```
Jika directory tidak ditemukan, maka akan melakukan `return`. Kemudian, dp berperan sebagai pointer ke setiap entri dalam direktori dir (players_path) yang dibaca menggunakan fungsi `readdir()`. Terdapat if condition jika file bertipe reguler (`dp->d_type == DT_REG`) dan tidak ditemukan string `"ManUtd"` pada nama filenya maka kondisi akan terpenuhi dan melakukan `sprintf` yang berisi players_path dan nama file atau `/home/grace/modul2/players/{nama file}` yang dimasukkan ke array path. Dilakukan `fork()` pada pid, jika `pid == 0` maka akan memanggil fungsi rm_file yang akan menghapus file-file yang telah dimasukkan array path tadi (file yang tidak memenuhi kondisi), program akan melakukan exit ketika sukses dilakukan. Setelah itu, menggunakan `waitpid` untuk menunggu child process selesai dieksekusi kemudian directory yang dibuka tadi akan ditutup menggunakan fungsi `closedir()`.

#### Pemanggilan fungsi
Untuk menjalankan fungsi yang memenuhi nomor 3B ini dilakukan pemanggilan `rm_notManUtd()` di dalam `int main()`
```c
// int main()
{
	...
	rm_notManUtd();
	...
}
```

### Pembahasan 3C

Diminta untuk mengkategorikan pemain ke dalam folder berdasarkan posisi yaitu Kiper, Bek, Gelandang, dan Penyerang

```c
void filter_posisi()
{
    char path[1000];
    char source[1000];
    char dest[1000];
    struct dirent *dp;
    DIR *dir = opendir(players_path);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (dp->d_type == DT_REG)
        {
            sprintf(source,"%s/%s",players_path,dp->d_name);
	...
	...
```
Membuat fungsi `filter_posisi` untuk melakukan pengkategorian pemain. Fungsi ini sedikit mirip dengan fungsi `rm_notManUtd()` pada bagian awalnya yaitu deklarasi array path, source, dest kemudian membuka directory players_path dan membaca directory tersebut. Jika kondisi terpenuhi (tipe file adalah reguler), maka akan `sprintf` "players_path/nama file" atau `/home/grace/modul2/players/{namafile}` seperti sebelumnya dan memasukannya ke array source. 
```c
			      // ...
			      int position_found = 0;
            char* pos = "none";
            if (strstr(dp->d_name, "Kiper") != NULL) 
            {
              position_found = 1;
              pos = "Kiper";
            }
            else if (strstr(dp->d_name, "Bek") != NULL) 
            {
              position_found = 1;
              pos = "Bek";
            } else if (strstr(dp->d_name, "Gelandang") != NULL) 
            {
              position_found = 1;
              pos = "Gelandang";
            } else if (strstr(dp->d_name, "Penyerang") != NULL) 
            {
              position_found = 1;
              pos = "Penyerang";
            }
		...
		...
```
Set variabel `position_found` menjadi 0 dan variabel `pos` menyimpan string "none". Dengan if-else, lakukan pengecekan string posisi pemain menggunakan fungsi `strstr()`
>- Jika ditemukan string `"Kiper"` pada nama file maka set nilai `position_found` menjadi 1 dan string `pos` menjadi `"Kiper"`
>- Jika ditemukan string `"Bek"` pada nama file maka set nilai `position_found` menjadi 1 dan string `pos` menjadi `"Bek"`
>- Jika ditemukan string `"Gelandang"` pada nama file maka set nilai `position_found` menjadi 1 dan string `pos` menjadi `"Gelandang"`
>- Jika ditemukan string `"Penyerang"` pada nama file maka set nilai `position_found` menjadi 1 dan string `pos` menjadi `"Penyerang"`

```c
			...
			pid_t pid;
            if(position_found)
            {
              pid = fork();
              sprintf(dest, "%s/%s", players_path, pos);
              if(pid == 0)
              {
                mv_file(source, dest);
                exit(EXIT_SUCCESS);
              }
            }
            waitpid(pid, NULL, 0);
        }
    }
}
```
Deklarasi pid untuk melakukan `fork()`. Jika position_found bernilai 1 maka kondisi terpenuhi dan melakukan `sprintf` string players_path/pos atau `/home/grace/modul2/players/{pos}` dan disimpan ke array dest. Jika `pid == 0` maka akan memanggil fungsi `mv_file` untuk memindahkan file dari array source tadi ke array yang menyimpan string dest sehingga:
>- File yang mengandung string `"Kiper"` akan dipindah ke `/home/grace/modul2/players/Kiper`
>- File yang mengandung string `"Bek"` akan dipindah ke `/home/grace/modul2/players/Bek`
>- File yang mengandung string `"Gelandang"` akan dipindah ke `/home/grace/modul2/players/Gelandang`
>- File yang mengandung string `"Penyerang"` akan dipindah ke `/home/grace/modul2/players/Penyerang`

Exit program jika sukses kemudian melakukan waitpid untuk menunggu child process selesai berjalan.

#### Pemanggilan Fungsi
Untuk menjalankan fungsi di atas maka akan dipanggil di `int main()` seperti ini
```c
// int main()
{
	...
	mk_dir("/home/grace/modul2/players/Kiper/");
  	mk_dir("/home/grace/modul2/players/Bek/");
  	mk_dir("/home/grace/modul2/players/Gelandang/");
  	mk_dir("/home/grace/modul2/players/Penyerang/");
  	filter_posisi();
	...
}
```
Membuat folder `Kiper`,`Bek`,`Gelandang`,`Penyerang` terlebih dahulu untuk menyimpan tiap kategori posisi pemain kemudian memanggil fungsi yang akan memfilter posisi pemain dengan cara `filter_posisi()`.

### Pembahasan 3D

Diminta membuat file dengan format `Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt` yang berisi kesebelasan terbaik berdasarkan rating dan wajib ada kiper, bek, gelandang, dan penyerang.

```c
void buatTim(int bek, int gelandang, int penyerang)
{
  pid_t pid;
  int status;
  char *args[5];
  args[0] = "bash";
  args[1] = "-c";
  args[4] = NULL;
  ...
```
Membuat fungsi `buatTim` dengan parameter jumlah bek, gelandang, penyerang. Di dalam fungsi ini awalnya dilakukan deklarasi pid, status, dan args[5]. Pada args index 0 atau args[0] menyimpan string `"bash"`, args[1] menyimpan string `"-c"`, dan args[4] menyimpan NULL.
```c
//mencari kiper terbaik
  ...
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Kiper | sort -t _ -k 4 -n -r | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);
  ...
```
Program di atas akan mencari kiper terbaik. Perintah `args[2] = (char*) malloc(200 * sizeof(char));` mengalokasikan memori sebesar 200 byte pada array args pada indeks ke-2. Kemudian, melakukan `sprintf` yang berisi args[2] tadi dan command `"ls ./modul2/players/Kiper | sort -t _ -k 4 -n -r | head -n 1 >> ./Formasi_%d_%d_%d.txt"`

>- `ls ./modul2/players/Kiper` akan menampilkan list semua file yang ada di dalam directory Kiper.
>- `sort -t _ -k 4` akan mengurutkan file dengan delimiter "_" dan kolom ke-4 yaitu rating. `-n` akan mengurutkan rating pemain secara numerikal dari yang paling kecil dan `-r` akan mereverse urutan tadi sehingga menjadi terurut dari yang ratingnya paling besar. 
>- `head -n 1` berarti akan mengambil satu baris pertama paling atas dari list
>- `>>./Formasi_%d_%d_%d.txt"` akan memasukkan hasilnya ke dalam file txt yang memiliki format `Formasi_%d_%d_%d.txt`, %d, %d, %d tersebut menyimpan dan menampilkan nilai integer jumlah bek, gelandang, penyerang yang diinginkan.

Deklarasi pid untuk melakukan `fork()`, jika `pid == 0` maka akan melakukan `execvp()` dengan parameter args[0] dan args tadi. Jika `pid < 0` maka akan exit program dan selain kondisi tadi akan dilakukan wait untuk menunda parent process sebelum child process selesai dieksekusi. Digunakan `free(args[2])` untuk membebaskan allocated memory args[2].

```c
//mencari bek terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Bek | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);

  //mencari gelandang terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Gelandang | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);
  
  // mencari penyerang terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Penyerang | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);

}
```
Pencarian bek, gelandang, penyerang terbaik memiliki cara yang sama dengan pencarian kiper terbaik. Perbedaannya terletak pada `head -n %d` dimana %d disesuaikan dengan jumlah posisi yang dicari dan terdapat tambahan 1 variabel ketika pemanggilan variabel sesuai dengan posisi yang dicari juga. Misalnya, jika mencari 3 gelandang terbaik dengan 4 bek dan 3 penyerang maka akan menjadi `head -n 3 >> ./Formasi_4_3_3.txt", gelandang, bek, gelandang, penyerang)`

#### Pemanggilan Fungsi
Untuk menjalankan fungsi `buatTim()` maka dilakukan pemanggilan di `int main()` juga dengan parameternya diisi jumlah bek, gelandang, penyerang yang diinginkan. Misalnya, jika ingin membuat kesebelasan terbaik dengan 1 kiper, 4 bek, 3 gelandang, dan 3 penyerang maka dilakukan pemanggilan seperti berikut:
```c
// int main
	...
	buatTim(4, 3, 3);
}
```
### Pembahasan No. 4

**Fungsi exe**
```c
void exe(char *command) //menerima perintah yang akan dieksekusi shell
{
    if (execl("/bin/sh", "/bin/sh", "-c", command, NULL) == -1) {
        printf("Error executing shell script file");
        exit(EXIT_FAILURE);
    }
}
```
Kode ini merupakan fungsi yang bernama ```exe``` yang berguna untuk mengeksekusi ```command```. Fungsi ini menerima satu argumen string "command" yang berisi perintah yang akan dijalankan di dalam shell. Fungsi ini menggunakan fungsi ```execl()``` untuk menjalankan program shell. Perintah shell akan dieksekusi dengan menjalankan ```/bin/sh``` dengan argumen ```-c``` diikuti oleh nilai argumen ```command```. Fungsi ini mengembalikan nilai -1 jika terjadi kesalahan dalam menjalankan program shell. Jika hal ini terjadi, fungsi ```execute``` akan mencetak pesan kesalahan dan mengakhiri program dengan status ```EXIT_FAILURE```.

**Fungsi sch**
```c
void sch(int second, int minute, int hour, char *command) //melakukan crontab 
{
    time_t now; //nyimpan waktu saat ini
    struct tm *curr_time, *count_time; // dua pointer tipe tm
    count_time = (struct tm*)malloc(sizeof(struct tm));
    memset(count_time, 0, sizeof(struct tm)); //struct countime jadi nol pake memset
    pid_t pid;
    
    while (1) {

        double diff=0; //perbedaan waktu curr dan count
        int interval=0; //nyimpan waktu interval crontab sejak dieksekusi pertama

        now = time(NULL);
        curr_time = localtime(&now);
        count_time->tm_year = curr_time->tm_year;
        count_time->tm_mon = curr_time->tm_mon;
        count_time->tm_mday = curr_time->tm_mday;
        count_time->tm_sec = (second == -1) ? curr_time->tm_sec : second;
        count_time->tm_min = (minute == -1) ? curr_time->tm_min : minute;
        count_time->tm_hour = (hour == -1) ? curr_time->tm_hour : hour;

        time_t count_t = mktime(count_time); //count_time akan diubah menjadi time t value dengan fungsi mktime dan disimpan pada variabel count_t
        diff += difftime(count_t, now); ///dihitung perbedaan

        if(hour == -1){
            if(minute == -1){

                // Tipe Cron 1: \* \* \* melakukan crontab setiap detik, diff slalu 0
                if(second == -1){
                    interval+=1;
                }

                // Tipe Cron 2: \* \* %S mengeksekusi command setiap satu menit sekali pada detik ke S
                else{

                    if(diff == 0) 
                        interval+=60; //akan jadi 60 detik
                    
                    else if(diff < 0)
                        diff = 60 + diff; //count < curr jadi 60 + diff

                }

            }else{

                // Tipe Cron 3: \* %M \* eksekusi comman tiap detik apabila nilai menitnya adalah M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff < 0)
                        diff = 3600 + diff - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
                    
                }

                // Tipe Cron 4: \* %M %S eksekusi command tiap jam pada menit ke M detik ke S
                else{

                    if(diff == 0)
                        interval+=3600;
                    
                    else if(diff < 0)
                        diff = 3600 + diff;
                    
                }
            }

        }else{

            if(minute == -1){

                // Tipe Cron 5: %H \* \* eksekusi comman tiap detik apabilai nilai jam adalah H
                if(second == -1){

                    if(diff == 0){
                        if(curr_time->tm_min == 59 && curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59*60 - 59;
                        else
                            interval+=1;
                    }
                    else if(diff < 0)
                        diff = 3600*24 + diff - 60*curr_time->tm_min - curr_time->tm_sec;
                    else
                        diff = diff - 60*curr_time->tm_min - curr_time->tm_sec;
                    
                }

                // TIpe Cron 6: %H \* %S eksekusi command tiap menit apabila jam adalah H dan detik adalah S
                else{

                    if(diff == 0){
                        if(curr_time->tm_min == 59)
                            interval = interval + 3600*23 + 60;
                        else
                            interval+=60;
                        
                    }else if(diff < 0){

                        if (diff > -60){

                            if(curr_time->tm_min == 59)
                                diff = 3600*23 + 60 + diff;
                            else
                                diff = 60 + diff;

                        }else
                            diff = 3600*(23-(curr_time->tm_hour-hour)) + 60*(60-curr_time->tm_min) + (second-curr_time->tm_sec); 
                        
                    }else
                        diff = diff - 60*curr_time->tm_min;
                }

            }else{

                // Tipe Cron 7: %H %M \* eksekusi command tiap detik apabila nilai jam adalah H dan nilai menit M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff<0)
                        diff = 3600*(24-(curr_time->tm_hour-hour)) + 60*(minute-curr_time->tm_min) - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
                        
                }

                // Tipe Cron 8: %H %M %S eksekusi command tiap satu hari sekali pada detik ke S, menit ke M dan jam ke H
                else{

                    if(diff == 0)
                        interval+=3600*24;
                        
                    else if(diff < 0)
                        diff = 3600*24 + diff;
                    
                }
            }
        }
    
        if (count_time->tm_sec == curr_time->tm_sec &&
            count_time->tm_min == curr_time->tm_min &&
            count_time->tm_hour == curr_time->tm_hour 
            ) // cek apakan curr dan count nya sama
            
            {
                pid = fork(); 
                if (pid == -1) {
                    printf("Schedule fork error");
                    exit(EXIT_FAILURE);
                }
                else if (pid == 0)
                    exe(command);
                else 
                    waitpid(pid, NULL,0);
        }
        printf("Current time: %s", ctime(&now));
        printf("Interval: %d\n", interval);
        printf("Diff: %d\n", (int)diff);
        printf("Program sleep for %d seconds\n",interval+(int)diff);
        printf("\n");

        sleep(interval+(int)diff);
    }
}
```
Kode ini merupakan fungsi yang bernama ```exe``` berguna untuk melakukan crontab dengan CPU state minimum dengan cara melakukan sleep. Fungsi ini memiliki empat parameter yaitu ```second```, ```minute```, ```hour``` dan ```command```.

```c
time_t now; //nyimpan waktu saat ini
    struct tm *curr_time, *count_time; // dua pointer tipe tm
    count_time = (struct tm*)malloc(sizeof(struct tm));
    memset(count_time, 0, sizeof(struct tm)); //struct countime jadi nol pake memset
    pid_t pid;
```
Kode di atas merupakan potongan kode dari fungsi ```exe```. Dalam ini program, kita akan mendeklarasikan variabel now bertipe ```time_t``` untuk menyimpan waktu saat ini. Selanjutnya, kita akan mendeklarasikan dua pointer bertipe struct tm untuk menyimpan informasi tanggal dan waktu. Pointer pertama ```curr_time```, akan digunakan untuk menyimpan waktu saat ini. Sementara pointer kedua, ```count_time```, akan digunakan nanti untuk menyimpan waktu konfigurasi crontab. Kita akan mengalokasikan memori untuk struct ```count_time``` menggunakan fungsi ```malloc()```. Selanjutnya, kita akan menginisialisasi struct ```count_time``` menjadi nol menggunakan fungsi ```memset()```. Selain itu, kita juga akan mendeklarasikan variabel ```pid``` bertipe ```pid_t``` untuk menyimpan pid dari child process.

```c
while (1) {

        double diff=0; //perbedaan waktu curr dan count
        int interval=0; //nyimpan waktu interval crontab sejak dieksekusi pertama

        now = time(NULL);
        curr_time = localtime(&now);
        count_time->tm_year = curr_time->tm_year;
        count_time->tm_mon = curr_time->tm_mon;
        count_time->tm_mday = curr_time->tm_mday;
        count_time->tm_sec = (second == -1) ? curr_time->tm_sec : second;
        count_time->tm_min = (minute == -1) ? curr_time->tm_min : minute;
        count_time->tm_hour = (hour == -1) ? curr_time->tm_hour : hour;

        time_t count_t = mktime(count_time); //count_time akan diubah menjadi time t value dengan fungsi mktime dan disimpan pada variabel count_t
        diff += difftime(count_t, now); ///dihitung perbedaan
```

Kode di atas merupakan ```while loop``` dari fungsi ```sch```.  Kode ini adalah mendeklarasikan beberapa variabel. Variabel ```diff``` akan digunakan untuk menyimpan perbedaan waktu antara ```curr_time``` dan ```count_time```. Awalnya, variabel ini akan diinisialisasi dengan nilai 0. Selanjutnya, variabel ```interval``` akan digunakan untuk menyimpan waktu interval crontab sejak eksekusi pertama crontab. Variabel ini juga akan diinisialisasi dengan nilai 0. Pointer ```curr_time``` akan diatur dengan nilai waktu lokal saat ini. Nilai ```year```, ```month```, dan ```day``` pada pointer ```count_time``` akan diatur agar sama seperti nilai tersebut pada ```curr_time```. Nilai ```sec```, ```min```, dan ```hour``` pada pointer ```count_time``` akan diperiksa terlebih dahulu apakah bernilai -1. Jika iya, nilai tersebut akan diatur agar sama seperti nilai tersebut pada ```curr_time```. Jika tidak, nilai tersebut akan diatur berdasarkan parameter dari fungsi schedule ini. Setelah nilai ```count_time``` diatur, pointer tersebut akan diubah menjadi ```time_t``` value menggunakan fungsi ```mktime()``` dan disimpan dalam variabel ```count_t```. Selanjutnya, perbedaan waktu antara ```count_t``` dan ```now``` akan dihitung menggunakan fungsi ```difftime()```, dan hasilnya akan disimpan dalam variabel ```diff```. Fungsi ```difftime()``` akan mengurangi nilai ```count_t``` dengan nilai ```now```, dan hasilnya akan dinyatakan dalam satuan detik.

Dalam fungsi ```sch``` terdapat juga 8 tipe cron sebagai konfigurasi sleep time.

1. Tipe 1 : ***
```c
// Tipe Cron 1: \* \* \* melakukan crontab setiap detik, diff slalu 0
                if(second == -1){
                    interval+=1;
                }

```
Arti dari ```* * *``` dalam konteks crontab adalah menjalankan tugas secara berkala setiap detik, sehingga intervalnya selalu 1 dan perbedaan waktu (diff) selalu 0.

2. Tipe 2 : * * %S
```c
// Tipe Cron 2: \* \* %S mengeksekusi command setiap satu menit sekali pada detik ke S
                else{

                    if(diff == 0) 
                        interval+=60; //akan jadi 60 detik
                    
                    else if(diff < 0)
                        diff = 60 + diff; //count < curr jadi 60 + diff

                }

```
Arti dari ```* * %S``` dalam konteks crontab adalah menjalankan perintah (command) setiap satu menit pada detik ke-S dalam setiap menit. 

Jika perbedaan waktu (diff) antara waktu saat ini (curr_time) dan nilai detik pada waktu yang diatur untuk menjalankan tugas (count_time) lebih besar dari 0, maka waktu tidur (sleep time) akan disetel sesuai dengan nilai ```diff``` tersebut. Jika ```diff``` sama dengan 0, maka ```interval``` waktu antara eksekusi perintah akan menjadi 60 detik, karena nilai detik pada ```count_time``` sama dengan ```curr_time```, sehingga perintah akan dijalankan 60 detik kemudian. Jika ```diff``` lebih kecil dari 0, artinya nilai detik pada ```count_time``` lebih kecil dari ```curr_time```, maka nilai ```diff``` yang sebenarnya akan menjadi ```60 + diff```.

3. Tipe 3 : * %M *
```c
 // Tipe Cron 3: \* %M \* eksekusi comman tiap detik apabila nilai menitnya adalah M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff < 0)
                        diff = 3600 + diff - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
```
Arti dari ```* %M *``` adalah command akan dieksekusi setiap detik jika nilai menitnya sama dengan M.

Jika nilai ```diff``` adalah 0 dan nilai detik dari ```curr_time``` adalah 59, maka nilai ```interval``` akan menjadi 3600 - 59 (waktu sekarang ditambah satu jam dan dikurangi 59 detik agar cron berjalan pada detik 0 pada menit M).
Jika nilai ```diff``` adalah 0 dan nilai detik dari ```curr_time``` bukan 59, maka nilai interval akan menjadi 1 karena cron akan berjalan setiap detik.
Jika nilai ```diff``` kurang dari 0, artinya nilai menit pada ```count_time``` kurang dari nilai menit ```curr_time```. Oleh karena itu, nilai ```diff``` yang sebenarnya adalah ```3600 + diff``` dikurangi nilai detik ```curr_time```.
Jika nilai ```diff``` lebih dari 0, artinya nilai menit pada ```count_time``` lebih dari nilai menit ```curr_time```. Oleh karena itu, nilai ```diff``` akan menjadi ```diff``` dikurangi nilai detik ```curr_time```.

4. Tipe 4 : * %M %S
```c
 // Tipe Cron 4: \* %M %S eksekusi command tiap jam pada menit ke M detik ke S
                else{

                    if(diff == 0)
                        interval+=3600;
                    
                    else if(diff < 0)
                        diff = 3600 + diff;

```
Arti dari ```* %M %S``` adalah menjalankan perintah setiap jam pada menit ke M dan detik ke S.

Jika selisih waktu (diff) lebih besar dari 0, maka nilai ```count_time``` akan lebih besar dari waktu saat ini (curr_time). Oleh karena itu, waktu tidur (sleep time) akan diatur sesuai dengan nilai diff itu sendiri.
Jika selisih waktu (diff) sama dengan 0, nilai intervalnya akan diatur sebagai 3600. Hal ini terjadi karena perintah dieksekusi setiap jam.
Jika selisih waktu (diff) lebih kecil dari 0, nilai ```count_time``` akan lebih kecil dari waktu saat ini (curr_time). Oleh karena itu, nilai diff yang sebenarnya akan dihitung sebagai 3600 ditambah diff.

5. Tipe 5 : %H * *
```c
// Tipe Cron 5: %H \* \* eksekusi comman tiap detik apabilai nilai jam adalah H
                if(second == -1){

                    if(diff == 0){
                        if(curr_time->tm_min == 59 && curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59*60 - 59;
                        else
                            interval+=1;
                    }
                    else if(diff < 0)
                        diff = 3600*24 + diff - 60*curr_time->tm_min - curr_time->tm_sec;
                    else
                        diff = diff - 60*curr_time->tm_min - curr_time->tm_sec;
```

Arti dari ```%H * *``` adalah menjalankan perintah setiap detik jika jam pada waktu saat ini adalah H.

Jika selisih waktu (diff) sama dengan 0 dan nilai menit dan detik dari waktu saat ini (curr_time) adalah 59, maka nilai ```interval``` akan dihitung sebagai 3600 * 24 - 5960 - 59 (waktu saat ini ditambah satu hari dikurangi 59 menit dan dikurangi 59 detik agar cron berjalan mulai dari detik ke-0 dan menit ke-0 pada jam H).
Jika selisih waktu (diff) sama dengan 0 dan nilai menit dan detik dari waktu saat ini tidak sama dengan 59, maka nilai ```interval``` akan diatur sebagai 1 karena perintah akan dieksekusi setiap detik.
Jika selisih waktu (diff) lebih kecil dari 0, maka nilai ```count_time``` akan lebih kecil dari waktu saat ini (curr_time). Oleh karena itu, nilai ```diff``` yang sebenarnya akan dihitung sebagai 3600 * 24 + ```diff``` - 60 * nilai menit dari ```curr_time``` - nilai detik dari ```curr_time```.
Jika selisih waktu (diff) lebih besar dari 0, maka nilai ```count_time``` akan lebih besar dari waktu saat ini (curr_time). Oleh karena itu, nilai ```diff``` akan diatur sebagai ```diff``` dikurangi 60 kali nilai menit dari ```curr_time``` dikurangi nilai detik dari ```curr_time```.

6. Tipe : %H * %S
```c
// TIpe Cron 6: %H \* %S eksekusi command tiap menit apabila jam adalah H dan detik adalah S
                else{

                    if(diff == 0){
                        if(curr_time->tm_min == 59)
                            interval = interval + 3600*23 + 60;
                        else
                            interval+=60;
                        
                    }else if(diff < 0){

                        if (diff > -60){

                            if(curr_time->tm_min == 59)
                                diff = 3600*23 + 60 + diff;
                            else
                                diff = 60 + diff;

                        }else
                            diff = 3600*(23-(curr_time->tm_hour-hour)) + 60*(60-curr_time->tm_min) + (second-curr_time->tm_sec); 
                        
                    }else
                        diff = diff - 60*curr_time->tm_min;
                }
```
Arti dari ```%H * %S``` adalah menjalankan perintah setiap menit jika jam pada waktu saat ini adalah H dan detiknya adalah S.

Jika selisih waktu (diff) sama dengan 0 dan nilai menit dari waktu saat ini (curr_time) adalah 59, maka nilai interval akan dihitung sebagai 3600 kali 23 ditambah 60 (waktu saat ini ditambah satu hari dikurangi 1 jam dan ditambah 1 menit agar cron berjalan mulai dari menit ke-0 pada jam H dan detik ke S).
Jika selisih waktu (diff) sama dengan 0 dan nilai menit dari waktu saat ini tidak sama dengan 59, maka nilai ```interval``` akan diatur sebagai 60 karena perintah akan dieksekusi setiap menit.
Jika selisih waktu (diff) lebih kecil dari 0, maka nilai ```count_time``` akan lebih kecil dari waktu saat ini (curr_time).

Jika nilai ```diff``` lebih besar dari -60 dan nilai menit dari waktu saat ini adalah 59, maka nilai ```diff``` akan dihitung sebagai 3600 kali 23 ditambah 60 ditambah ``diff```.
Jika nilai ```diff``` lebih besar dari -60 dan nilai menit dari waktu saat ini tidak sama dengan 59, maka nilai diff akan dihitung sebagai 60 ditambah ```diff```.

Jika nilai ```diff``` lebih besar dari 0, maka nilai ```count_time``` akan lebih besar dari waktu saat ini (curr_time). Oleh karena itu, nilai ```diff``` akan diatur sebagai ```diff``` dikurangi 60 kali nilai menit dari ```curr_time```.

7. Tipe 7 : %H %M *
```c
// Tipe Cron 7: %H %M \* eksekusi command tiap detik apabila nilai jam adalah H dan nilai menit M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff<0)
                        diff = 3600*(24-(curr_time->tm_hour-hour)) + 60*(minute-curr_time->tm_min) - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
```
Arti dari ```%H %M *``` dalam konfigurasi cron job adalah menjalankan perintah setiap detik ketika jam (hour) memiliki nilai ```H``` dan menit (minute) memiliki nilai ```M```. 

Jika selisih waktu (diff) antara waktu saat ini (curr_time) dan waktu yang ditentukan (count_time) adalah 0, dan nilai detik dari ```curr_time``` adalah 59, maka nilai ```interval``` akan dihitung sebagai 360024 - 59 (untuk menjalankan cron job mulai dari detik 0 pada jam H dan menit M pada hari berikutnya). Jika ```diff = 0``` dan nilai detik dari ```curr_time``` tidak sama dengan 59, maka nilai ```interval``` akan menjadi 1 (untuk menjalankan cron job setiap detik). Jika ```diff < 0```, yang berarti ```count_time``` sudah berlalu dari ```curr_time```, maka nilai diff yang sebenarnya dihitung sebagai 3600(24- (nilai jam curr_time - H)) + 60*(nilai menit - nilai menit curr_time) - nilai detik ```curr_time```. Dan jika ```diff > 0```, yang berarti ```count_time``` masih akan datang setelah ```curr_time```, maka nilai ```diff``` dihitung sebagai diff - nilai detik ```curr_time```.

8. Tipe 8 : %H %M %S
```c
// Tipe Cron 8: %H %M %S eksekusi command tiap satu hari sekali pada detik ke S, menit ke M dan jam ke H
                else{

                    if(diff == 0)
                        interval+=3600*24;
                        
                    else if(diff < 0)
                        diff = 3600*24 + diff;

```
Arti dari ```%H %M %S``` dalam konfigurasi cron job adalah menjalankan perintah setiap satu hari sekali pada detik ke S, menit ke M, dan jam ke H. 

Jika selisih waktu (diff) antara waktu saat ini (curr_time) dan waktu yang ditentukan (count_time) adalah lebih besar dari 0, berarti ```count_time``` akan datang setelah ```curr_time```, maka ```sleep time``` (waktu jeda) akan dihitung sebagai nilai diff itu sendiri. Jika ```diff = 0```, nilai interval akan dihitung sebagai 3600 dikali 24 (satu hari penuh), karena nilai detik dari ```count_time``` sama dengan ```curr_time```, sehingga perintah akan dieksekusi satu hari setelahnya. Jika ```diff < 0```, berarti ```count_time``` sudah berlalu dari ```curr_time```, maka nilai ```diff``` yang sebenarnya dihitung sebagai 3600 * 24 + ```diff``` (untuk menghitung selisih waktu antara curr_time dan count_time pada hari berikutnya).

Selain fungsi tipe cron di dalam fungsi ```sch``` juga memiliki kode untuk eksekusi command yaitu :

```c
if (count_time->tm_sec == curr_time->tm_sec &&
            count_time->tm_min == curr_time->tm_min &&
            count_time->tm_hour == curr_time->tm_hour 
            ) // cek apakan curr dan count nya sama
            
            {
                pid = fork(); 
                if (pid == -1) {
                    printf("Schedule fork error");
                    exit(EXIT_FAILURE);
                }
                else if (pid == 0)
                    exe(command);
                else 
                    waitpid(pid, NULL,0);
```
Potongan kode ini memulai dengan melakukan pengecekan apakah nilai detik, menit, dan jam pada variabel ```curr_time``` dan ```count_time``` sama. Jika kondisi ini terpenuhi, maka program akan membuat sebuah child process dengan menggunakan fungsi ```fork()```, dan menyimpan PID dari child process tersebut ke dalam variabel pid. Selanjutnya, dilakukan pengecekan apakah nilai ```pid``` sama dengan -1. Jika ya, artinya ```fork()``` gagal, dan program akan mencetak pesan error dan menghentikan eksekusi program. Selanjutnya, dilakukan pengecekan apakah nilai pid sama dengan 0. Jika ya, artinya ```fork()``` berhasil, dan program akan mengeksekusi perintah dengan memanggil fungsi ```execute()```. Jika nilai ```pid``` lebih besar dari 0, artinya ```fork()``` berhasil, dan parent process akan menunggu child process untuk menyelesaikan eksekusi perintah dengan menggunakan fungsi ```waitpid()```.

Fungsi ```sch``` terdapat kode untuk set sleep time. Kodenya terdapat di bawah ini :
```c
 printf("Current time: %s", ctime(&now));
        printf("Interval: %d\n", interval);
        printf("Diff: %d\n", (int)diff);
        printf("Program sleep for %d seconds\n",interval+(int)diff);
        printf("\n");

        sleep(interval+(int)diff);
```
Untuk memudahkan proses debugging pada 8 jenis konfigurasi kron, program akan mencetak pesan yang berisi informasi mengenai durasi sleep yang akan dilakukan. Durasi sleep ini ditentukan dengan menjumlahkan nilai interval dan nilai diff agar sesuai dengan konfigurasi yang diatur.

**Fungsi check_arg**
```c
int check_arg(char *arg, int min, int max)
{
    if (strcmp(arg, "*") == 0) {
        return -1;
    }

    int value = atoi(arg); //assign yang awalnya string jd integer fungsi atoi
    if (value < min || value > max) { //cek apakah di range
        printf("Invalid argument value");
        exit(EXIT_FAILURE);
    }

    return value;
}
```
Fungsi ini menerima tiga parameter, yaitu argumen input, nilai minimum argumen, dan nilai maksimum argumen. Pertama, fungsi akan melakukan pengecekan apakah input argumen sama dengan "*". Jika benar, fungsi akan mengembalikan nilai -1. Selanjutnya, akan ada variabel bernama ```value``` yang akan diisi dengan nilai integer dari input argumen yang awalnya dalam bentuk string, menggunakan fungsi ```atoi()```. Selanjutnya, fungsi akan melakukan pengecekan apakah input argumen berada dalam rentang nilai minimum dan maksimum. Jika input argumen di luar rentang tersebut, program akan mencetak pesan bahwa argumen tidak valid dan menghentikan eksekusi program. Namun, jika input argumen berada dalam rentang yang valid, program akan mengembalikan nilai dari variabel ```value```.

**Main**
```c
if (argc != 5) {
        printf("The arguments should be <Program> <Hour> <Minute> <Second> <Script File Directory>");
        exit(EXIT_FAILURE);
    }
```
Untuk mengecek argumen berjumlah 5

```c
//check argumen hour, minute, second terus hasil return disimpan di varible masing"
    int hour = check_arg(argv[1], 0, 23);
    int minute = check_arg(argv[2], 0, 59);
    int second =check_arg(argv[3], 0, 59);
    char *shellDir = argv[4]; //argumen path file shell disimpan pada ini
```
Menggunakan fungsi ```check_argument``` untuk memeriksa argumen ```hour```, ```minute```, dan ```second```, dan menyimpan hasil pengembaliannya pada variabel ```hour```, ```minute```, dan ```second```. Selanjutnya, menyimpan argumen ```path``` file.sh pada sebuah variabel string yang disebut ```shellDir```.

```c
//daemon
    pid_t pid, sid; 
    pid = fork(); 
    if (pid == -1) { 

        printf("Main fork error");
        exit(EXIT_FAILURE);

    } else if (pid == 0) { //fork berhasil
        setsid(); //membuat SID yang akan disimpan di variable sid
        if (sid < 0) { //sid gagal dibuat
            printf("Main setsid error");
            exit(EXIT_FAILURE);
        }
        sch(second, minute, hour, shellDir);
    }
    else
        exit(EXIT_SUCCESS); //pid > 0 fork berhasil sedang dijalankan parent process
```
Kode ini berguna untuk membuat daemon. Program akan membuat child process dengan menggunakan ```fork()``` dan menyimpan PID dari child process tersebut dalam variabel ```pid```. Selanjutnya, program akan melakukan beberapa pengecekan terhadap nilai ```pid```. Pertama, jika ```pid``` bernilai -1, maka ```fork()``` gagal dan program akan mencetak pesan error dan menghentikan eksekusi program. Selanjutnya, program akan memeriksa apakah ```pid``` bernilai 0, jika iya, itu berarti program sedang dijalankan dalam child process. Kemudian, program akan membuat Unique Session ID (SID) menggunakan ```setsid()``` dan menyimpannya dalam variabel ```sid```. Setelah itu, program akan memeriksa apakah ```sid``` kurang dari 0. Jika ```sid``` kurang dari 0, berarti SID gagal dibuat dan program akan mencetak pesan error dan menghentikan eksekusi program. Namun, jika ```sid``` tidak kurang dari 0, child process akan memanggil fungsi ```schedule()```. Selanjutnya, program akan memeriksa apakah ```pid``` lebih besar dari 0, jika iya, itu berarti program sedang dijalankan dalam parent process. Program akan mengakhiri eksekusi dengan status ```EXIT_SUCCESS```. Hal ini dilakukan karena parent process tidak memerlukan tindakan lebih lanjut setelah melakukan forking, dan child process akan berjalan secara independen.






