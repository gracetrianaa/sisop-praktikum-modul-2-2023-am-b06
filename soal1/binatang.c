#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#define _GNU_SOURCE

void download_file(char link[], char file_name[]) {

	int status;

	if (fork()==0) {
	char *argv[] = {"wget", link, "-O", file_name, NULL};
	execv("/bin/wget", argv);
	}
		while (wait(&status) >= 0);
		return;
}

void unzip(char zip_name[], char folder_name[]) {

	int status;

	if (fork()==0) {
	char *argv[] = {"unzip", zip_name, "-d", folder_name, NULL};
	execv("/usr/bin/unzip", argv);
	}
		while (wait(&status) >= 0);
		return;
}

void new_directory_HewanDarat() {

        if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", NULL};
        execv("/bin/mkdir", argv);
	}
}

void new_directory_HewanAmphibi() {

        if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
        }
}

void new_directory_HewanAir() {

	if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
        }

}

void zip_HewanDarat() {

	if (fork() == 0) {
	char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
	execv("/bin/zip", argv);
	}

}

void zip_HewanAmphibi() {

        if (fork() == 0) {
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
        }
}

void zip_HewanAir() {

	if (fork() == 0) {
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
        }
}

void select_random_file(char folder_path[]) {

	char buf[PATH_MAX];
	DIR* dir;
	struct dirent *ent;
	srand(time(NULL));
    	int n_files = 0;

    	dir = opendir(folder_path);
    	if (dir == NULL) {
        	fprintf(stderr, "Error opening directory: %s\n", folder_path);
        	return;
		}

    	while ((ent = readdir(dir)) != NULL) {
        	if (ent -> d_type == DT_REG) {
            	n_files++;
            		if (rand()%n_files == 0) {
                	snprintf (buf, sizeof(buf), "%s", ent -> d_name);
            		}
        	}
    	}
    	closedir(dir);
    	printf ("Penjagaan Hewan : %s\n",buf);
}

void move_filter_HewanDarat(){
	system("find . -name \"*_darat*\" -type f | awk '{ \
    		if (!system(\"[ ! -e HewanDarat/\" $0 \" ]\")) \
        	system(\"mv \" $0 \" HewanDarat/\") \
	}'");
}

void move_filter_HewanAir(){
        system("find . -name \"*_air*\" -type f | awk '{ \
                if (!system(\"[ ! -e HewanAir/\" $0 \" ]\")) \
                system(\"mv \" $0 \" HewanAir/\") \
        }'");
}

void move_filter_HewanAmphibi(){
        system("find . -name \"*_amphibi*\" -type f | awk '{ \
                if (!system(\"[ ! -e HewanAmphibi/\" $0 \" ]\")) \
                system(\"mv \" $0 \" HewanAmphibi/\") \
        }'");
}



int main () {
	download_file("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq","binatang.zip");
	unzip("binatang.zip","f_binatang");

	new_directory_HewanDarat();
	new_directory_HewanAmphibi();
	new_directory_HewanAir();

	char* folder_path = "/home/glenaya/Sisop/prak2/f_binatang";
	select_random_file(folder_path);

	move_filter_HewanDarat();
	move_filter_HewanAir();
	move_filter_HewanAmphibi();

	zip_HewanDarat();
        zip_HewanAmphibi();
        zip_HewanAir();
}
