#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>

void for_exec(char path[], char *argv[])
{
  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    execv(path,argv);
    exit(EXIT_SUCCESS);
  }
  else
  {
    while(wait(&status) > 0)
        ;
    return;
  }
}

void file_download(char urlFile[], char nameFile[])
{
  char *argv[] = {"wget", urlFile, "-O", nameFile, NULL};
  for_exec("/usr/bin/wget", argv);
}

void mk_dir(char dir[])
{
  char *argv[] = {"mkdir", "-p", dir, NULL};
  for_exec("/usr/bin/mkdir", argv);
}

void unzip(char nameFile[], char destDir[])
{
  char *argv[] = {"unzip", "-q", nameFile, "-d", destDir, NULL};
  for_exec("/usr/bin/unzip", argv);
}

void rm_file(char nameFile[])
{
  char *argv[] = {"rm", nameFile, NULL};
  for_exec("/usr/bin/rm", argv);
}

void mv_file(char *source, char *dest)
{
  char *argv[] = {"mv", source, dest, NULL};
  for_exec("/usr/bin/mv", argv);
}

// Penyelesaian 3B
char players_path[] = "/home/grace/modul2/players";
void rm_notManUtd()
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(players_path);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (dp->d_type == DT_REG && strstr(dp->d_name, "ManUtd") == NULL)
        {
            pid_t pid;
            sprintf(path,"%s/%s",players_path,dp->d_name);
            pid = fork();
            if(pid == 0)
            {
              rm_file(path);
              exit(EXIT_SUCCESS);
            }
            waitpid(pid, NULL, 0);
        }
    }

    closedir(dir);
}

// Penyelesaian 3C
void filter_posisi()
{
    char path[1000];
    char source[1000];
    char dest[1000];
    struct dirent *dp;
    DIR *dir = opendir(players_path);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (dp->d_type == DT_REG)
        {
            sprintf(source,"%s/%s",players_path,dp->d_name);

            int position_found = 0;
            char* pos = "none";
            if (strstr(dp->d_name, "Kiper") != NULL) 
            {
              position_found = 1;
              pos = "Kiper";
            }
            else if (strstr(dp->d_name, "Bek") != NULL) 
            {
              position_found = 1;
              pos = "Bek";
            } else if (strstr(dp->d_name, "Gelandang") != NULL) 
            {
              position_found = 1;
              pos = "Gelandang";
            } else if (strstr(dp->d_name, "Penyerang") != NULL) 
            {
              position_found = 1;
              pos = "Penyerang";
            }
            pid_t pid;
            if(position_found)
            {
              pid = fork();
              sprintf(dest, "%s/%s", players_path, pos);
              if(pid == 0)
              {
                mv_file(source, dest);
                exit(EXIT_SUCCESS);
              }
            }
            waitpid(pid, NULL, 0);
        }
    }
}

//Penyelesaian 3D
void buatTim(int bek, int gelandang, int penyerang)
{
  pid_t pid;
  int status;
  char *args[5];
  args[0] = "bash";
  args[1] = "-c";
  args[4] = NULL;

  //mencari kiper terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Kiper | sort -t _ -k 4 -n -r | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);

  //mencari bek terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Bek | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);

  //mencari gelandang terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Gelandang | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);
  
  // mencari penyerang terbaik
  args[2] = (char*) malloc(200 * sizeof(char));
  sprintf(args[2], "ls ./modul2/players/Penyerang | sort -t _ -k 4 -n -r | head -n %d >> ./Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
  pid = fork();
  if (pid == 0) {
    execvp(args[0], args);
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
      exit(EXIT_FAILURE);
  } else {
        while((wait(&status)) > 0);
  }
  free(args[2]);

}

int main(){
  file_download("https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download","players.zip");
  mk_dir("modul2");
  unzip("players.zip","modul2/");
  rm_file("players.zip");
  rm_notManUtd();
  mk_dir("/home/grace/modul2/players/Kiper/");
  mk_dir("/home/grace/modul2/players/Bek/");
  mk_dir("/home/grace/modul2/players/Gelandang/");
  mk_dir("/home/grace/modul2/players/Penyerang/");
  filter_posisi();
  buatTim(4, 3, 3);
}
