//Glenaya 5025211202

#include <signal.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <math.h>

void exe(char *command) //menerima perintah yang akan dieksekusi shell
{
    if (execl("/bin/sh", "/bin/sh", "-c", command, NULL) == -1) {
        printf("Error executing shell script file");
        exit(EXIT_FAILURE);
    }
}

void sch(int second, int minute, int hour, char *command) //melakukan crontab 
{
    time_t now; //nyimpan waktu saat ini
    struct tm *curr_time, *count_time; // dua pointer tipe tm
    count_time = (struct tm*)malloc(sizeof(struct tm));
    memset(count_time, 0, sizeof(struct tm)); //struct countime jadi nol pake memset
    pid_t pid;
    
    while (1) {

        double diff=0; //perbedaan waktu curr dan count
        int interval=0; //nyimpan waktu interval crontab sejak dieksekusi pertama

        now = time(NULL);
        curr_time = localtime(&now);
        count_time->tm_year = curr_time->tm_year;
        count_time->tm_mon = curr_time->tm_mon;
        count_time->tm_mday = curr_time->tm_mday;
        count_time->tm_sec = (second == -1) ? curr_time->tm_sec : second;
        count_time->tm_min = (minute == -1) ? curr_time->tm_min : minute;
        count_time->tm_hour = (hour == -1) ? curr_time->tm_hour : hour;

        time_t count_t = mktime(count_time); //count_time akan diubah menjadi time t value dengan fungsi mktime dan disimpan pada variabel count_t
        diff += difftime(count_t, now); ///dihitung perbedaan

        if(hour == -1){
            if(minute == -1){

                // Tipe Cron 1: \* \* \* melakukan crontab setiap detik, diff slalu 0
                if(second == -1){
                    interval+=1;
                }

                // Tipe Cron 2: \* \* %S mengeksekusi command setiap satu menit sekali pada detik ke S
                else{

                    if(diff == 0) 
                        interval+=60; //akan jadi 60 detik
                    
                    else if(diff < 0)
                        diff = 60 + diff; //count < curr jadi 60 + diff

                }

            }else{

                // Tipe Cron 3: \* %M \* eksekusi comman tiap detik apabila nilai menitnya adalah M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff < 0)
                        diff = 3600 + diff - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
                    
                }

                // Tipe Cron 4: \* %M %S eksekusi command tiap jam pada menit ke M detik ke S
                else{

                    if(diff == 0)
                        interval+=3600;
                    
                    else if(diff < 0)
                        diff = 3600 + diff;
                    
                }
            }

        }else{

            if(minute == -1){

                // Tipe Cron 5: %H \* \* eksekusi comman tiap detik apabilai nilai jam adalah H
                if(second == -1){

                    if(diff == 0){
                        if(curr_time->tm_min == 59 && curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59*60 - 59;
                        else
                            interval+=1;
                    }
                    else if(diff < 0)
                        diff = 3600*24 + diff - 60*curr_time->tm_min - curr_time->tm_sec;
                    else
                        diff = diff - 60*curr_time->tm_min - curr_time->tm_sec;
                    
                }

                // TIpe Cron 6: %H \* %S eksekusi command tiap menit apabila jam adalah H dan detik adalah S
                else{

                    if(diff == 0){
                        if(curr_time->tm_min == 59)
                            interval = interval + 3600*23 + 60;
                        else
                            interval+=60;
                        
                    }else if(diff < 0){

                        if (diff > -60){

                            if(curr_time->tm_min == 59)
                                diff = 3600*23 + 60 + diff;
                            else
                                diff = 60 + diff;

                        }else
                            diff = 3600*(23-(curr_time->tm_hour-hour)) + 60*(60-curr_time->tm_min) + (second-curr_time->tm_sec); 
                        
                    }else
                        diff = diff - 60*curr_time->tm_min;
                }

            }else{

                // Tipe Cron 7: %H %M \* eksekusi command tiap detik apabila nilai jam adalah H dan nilai menit M
                if(second == -1){

                    if(diff == 0){

                        if(curr_time->tm_sec == 59)
                            interval = interval + 3600*24 - 59;
                        else
                            interval+=1;

                    }
                    else if(diff<0)
                        diff = 3600*(24-(curr_time->tm_hour-hour)) + 60*(minute-curr_time->tm_min) - curr_time->tm_sec;
                    else
                        diff = diff - curr_time->tm_sec;
                        
                }

                // Tipe Cron 8: %H %M %S eksekusi command tiap satu hari sekali pada detik ke S, menit ke M dan jam ke H
                else{

                    if(diff == 0)
                        interval+=3600*24;
                        
                    else if(diff < 0)
                        diff = 3600*24 + diff;
                    
                }
            }
        }
    
        if (count_time->tm_sec == curr_time->tm_sec &&
            count_time->tm_min == curr_time->tm_min &&
            count_time->tm_hour == curr_time->tm_hour 
            ) // cek apakan curr dan count nya sama
            
            {
                pid = fork(); 
                if (pid == -1) {
                    printf("Schedule fork error");
                    exit(EXIT_FAILURE);
                }
                else if (pid == 0)
                    exe(command);
                else 
                    waitpid(pid, NULL,0);
        }
        printf("Current time: %s", ctime(&now));
        printf("Interval: %d\n", interval);
        printf("Diff: %d\n", (int)diff);
        printf("Program sleep for %d seconds\n",interval+(int)diff);
        printf("\n");

        sleep(interval+(int)diff);
    }
}

int check_arg(char *arg, int min, int max)
{
    if (strcmp(arg, "*") == 0) {
        return -1;
    }

    int value = atoi(arg); //assign yang awalnya string jd integer fungsi atoi
    if (value < min || value > max) { //cek apakah di range
        printf("Invalid argument value");
        exit(EXIT_FAILURE);
    }

    return value;
}

int main(int argc, char *argv[])
{
    if (argc != 5) {
        printf("The arguments should be <Program> <Hour> <Minute> <Second> <Script File Directory>");
        exit(EXIT_FAILURE);
    }
    
    //check argumen hour, minute, second terus hasil return disimpan di varible masing"
    int hour = check_arg(argv[1], 0, 23);
    int minute = check_arg(argv[2], 0, 59);
    int second =check_arg(argv[3], 0, 59);
    char *shellDir = argv[4]; //argumen path file shell disimpan pada ini

    //daemon
    pid_t pid, sid; 
    pid = fork(); 
    if (pid == -1) { 

        printf("Main fork error");
        exit(EXIT_FAILURE);

    } else if (pid == 0) { //fork berhasil
        setsid(); //membuat SID yang akan disimpan di variable sid
        if (sid < 0) { //sid gagal dibuat
            printf("Main setsid error");
            exit(EXIT_FAILURE);
        }
        sch(second, minute, hour, shellDir);
    }
    else
        exit(EXIT_SUCCESS); //pid > 0 fork berhasil sedang dijalankan parent process
    
}