#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <dirent.h>

void createKiller(char *argv[], pid_t pid, pid_t sid)
{
    FILE *fileptr = fopen("killer.c", "w");

    // Killer.c
    char input[1024] = ""
    "#include <unistd.h>\n"
    "#include <wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
    "}\n";

    // Mode A
    char command[1024];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fileptr, input, command, "/usr/bin/pkill");
    }

    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fileptr, input, command, "/bin/kill");
    }

    fclose(fileptr);

    // Compile killer.c
    pid = fork();
    if(pid == 0)
    {    
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", command);
    }
    while(wait(NULL) != pid);

    // Remove killer.c
    pid = fork();
    if (pid == 0) {
    	char *argv[] = {"rm", "killer.c", NULL};
    	execv("/bin/rm", argv);
    }
    while(wait(NULL) != pid);
}

int main(int argc, char *argv[]) {

  if(argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b')) {
    printf("Wrong Argument \nExecute With: ./lukisan -[a/b]\n");
    exit(0);
  }

  pid_t pid, sid;
  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();

  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  createKiller(argv, pid, sid);
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while(1) {
	pid_t child_id;
	int status;
	child_id = fork();
        time_t raw;
        struct tm *timeinfo;
        char dates[40];
        time(&raw);
        timeinfo = localtime(&raw);
        strftime(dates, sizeof(dates), "%Y-%m-%d_%H:%M:%S", timeinfo); 
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
		char *argv[4] = {"mkdir", "-p", dates, NULL};
		execv("/bin/mkdir", argv); 
	} else {
		while ((wait(&status)) > 0);
		pid_t child_id_2;
		int status2, i=0;
		child_id_2 = fork();
		if (child_id_2 == 0)
		{
			while (i<15)
			{
        			time_t rw;
        			struct tm *timeinfo2;
        			char tgl[40];
        			time(&rw);
        			timeinfo2 = localtime(&rw);
        			strftime(tgl, sizeof(tgl), "%Y-%m-%d_%H:%M:%S", timeinfo2); 
				char adrs[50];
				sprintf(adrs, "https://picsum.photos/%ld", ((rw%1000)+100));
				pid_t child_id_3;
				child_id_3 = fork();
				if (child_id_3 == 0) {
					chdir(dates);
				//	execl("/usr/bin/wget", "wget", "-O", tgl, adrs, NULL);
					execl("/usr/bin/wget", "wget", "-q","-O", tgl, adrs, NULL);
				}
				i++;
				sleep(5);
			}

			char zip_name[50];
			sprintf(zip_name, "%s.zip", dates);
                        char *argv2[5] = {"zip", "-rm", zip_name, dates, NULL};
                        execv("/usr/bin/zip", argv2);
		}
	}
	sleep(30);
  }
}
